<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use DateTimeInterface;
use Exception;
use Stringable;

/**
 * InformationInterface interface file.
 * 
 * This interface is for all informations that are gatherered. It permits to
 * have a single entry point for all type of informations.
 * 
 * @author Anastaszor
 */
interface InformationInterface extends Stringable
{
	
	/**
	 * The identifier of this specific information.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * The class of the object the information is extracted from.
	 *
	 * @return string
	 */
	public function getSupportClass() : string;
	
	/**
	 * Gets the date and time when this information was created.
	 *
	 * @return DateTimeInterface
	 */
	public function getCreationDate() : DateTimeInterface;
	
	/**
	 * Gets the current state of this information.
	 * 
	 * @return InformationStateInformation
	 */
	public function getState() : InformationStateInformation;
	
	/**
	 * Gets whether this information is cacheable. This is useful if there is
	 * a stream of informations that is processed and this information may be
	 * watched handled multiple times during the stream process, to improve
	 * processing performance.
	 * 
	 * @return boolean
	 */
	public function isCacheable() : bool;
	
	/**
	 * Gets a condensated representation of this information, as to compare if
	 * its internal state has changed since last time.
	 * 
	 * @return string
	 */
	public function getEtag() : string;
	
	/**
	 * Makes this interface be visited by the given visitor.
	 * 
	 * @template T
	 * @param InformationVisitorInterface<T> $visitor
	 * @return ?T
	 * @throws Exception if the visiting process could not be done
	 */
	public function beVisitedBy(InformationVisitorInterface $visitor);
	
}
