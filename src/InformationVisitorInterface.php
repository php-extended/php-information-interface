<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use Exception;
use Iterator;
use Stringable;

/**
 * InformationVisitorInterface interface file.
 * 
 * This interface is for any treatment that should be made on information
 * objects to discriminate them.
 * 
 * @author Anastaszor
 * @template T
 */
interface InformationVisitorInterface extends Stringable
{
	
	/**
	 * Processes the given array of informations. Behavior on failures is
	 * left to the implementation.
	 *
	 * @param array<InformationInterface> $informationIterator
	 * @return ?T
	 * @throws Exception if the visiting process could not be done
	 */
	public function visitAll(array $informationIterator);
	
	/**
	 * Processes the given iterator of informations. Behavior on failures is
	 * left to the implementation.
	 * 
	 * @param Iterator<InformationInterface> $informationIterator
	 * @return ?T
	 * @throws Exception if the visiting process could not be done
	 */
	public function visitIterator(Iterator $informationIterator);
	
	/**
	 * Makes this visitor visits an unspecified unique information.
	 * 
	 * @param InformationInterface $information
	 * @return ?T
	 * @throws Exception if the visiting process could not be done
	 */
	public function visitInformation(InformationInterface $information);
	
	/**
	 * Makes this visitor visits specifically an information of type triple.
	 * 
	 * @param InformationTripleInterface $information
	 * @return ?T
	 * @throws Exception if the visiting process could not be done
	 */
	public function visitTriple(InformationTripleInterface $information);
	
	/**
	 * Makes this visitor visits specifically an information of type object.
	 * 
	 * @param InformationObjectInterface $information
	 * @return ?T
	 * @throws Exception if the visiting process could not be done
	 */
	public function visitObject(InformationObjectInterface $information);
	
}
