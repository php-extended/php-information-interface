<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

/**
 * InformationObjectInterface interface file.
 * 
 * This interface is for all informations that are gathered. It permits to have
 * a single entry point for all type of informations, included informations
 * that have composite primary key.
 * 
 * @author Anastaszor
 */
interface InformationObjectInterface extends InformationInterface
{
	
	/**
	 * Gets all the datas that represent the primary key of the object.
	 * The key is the field name and the value is the field value.
	 * 
	 * @return array<string, string>
	 */
	public function getPrimaryKey() : array;
	
	/**
	 * Gets all the data that represent the primary key, except the values
	 * are normalized as md5 hashes.
	 * 
	 * @return array<string, string>
	 */
	public function getPrimaryKeyMd5() : array;
	
	/**
	 * Gets all the data that represent the primary key, except that fields
	 * that represent boolean, integer, float or date fields are kept as-is and
	 * the other are normalized as md5 hashes.
	 * 
	 * @return array<string, string>
	 */
	public function getSmartPrimaryKeyMd5() : array;
	
	/**
	 * Gets all the data that represent the primary key, except the values
	 * are normalized as sha1 hashes.
	 * 
	 * @return array<string, string>
	 */
	public function getPrimaryKeySha1() : array;
	
	/**
	 * Gets all the data that represent the primary key, except that fields
	 * that represent boolean, integer, float or date fields are kept as-is and
	 * the other are normalized as sha1 hashes. 
	 * 
	 * @return array<string, string>
	 */
	public function getSmartPrimaryKeySha1() : array;
	
	/**
	 * Gets all the datas that are gathered by this multi information.
	 * The key is the field name and the value is the field value.
	 *
	 * @return array<string, string>
	 */
	public function getInformationDatas() : array;
	
	/**
	 * Gets the data with the given name, null if does not exists.
	 * 
	 * @param ?string $name
	 * @return ?string
	 */
	public function getInformationData(?string $name) : ?string;
	
	/**
	 * Removes the given data from this object, true if it was existant.
	 * 
	 * @param ?string $name
	 * @return boolean
	 */
	public function removeInformationData(?string $name) : bool;
	
	/**
	 * Gets all the relations that are self related to this multi information.
	 * The key is the target relation name and the value is the target field value.
	 *
	 * @return array<string, string>
	 */
	public function getInformationRelations() : array;
	
	/**
	 * Gets the relation with the given name, null if does not exists.
	 * 
	 * @param ?string $name
	 * @return ?string
	 */
	public function getInformationRelation(?string $name) : ?string;
	
	/**
	 * Removes the given relation from this object, true if it was existant.
	 * 
	 * @param ?string $name
	 * @return boolean
	 */
	public function removeInformationRelation(?string $name) : bool;
	
}
