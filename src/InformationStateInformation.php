<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

use Stringable;

/**
 * InformationInterface interface file.
 *
 * This interface is for all informations that are gatherered. It permits to
 * have a single entry point for all type of informations.
 *
 * @author Anastaszor
 */
interface InformationStateInformation extends Stringable
{
	
	/**
	 * Gets whether the state of this information is pending, or processed.
	 * 
	 * @return boolean
	 */
	public function isPending() : bool;
	
	/**
	 * Gets whether the state of this information is rejected. If this
	 * information is not pending and not rejected, then it has been
	 * successfully treated.
	 * 
	 * @return boolean
	 */
	public function isRejected() : bool;
	
}
