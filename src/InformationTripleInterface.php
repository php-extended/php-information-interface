<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-information-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Information;

/**
 * InformationTripleInterface interface file.
 * 
 * This interface is for all informations that are gathered into semantic form.
 * 
 * @author Anastaszor
 */
interface InformationTripleInterface extends InformationInterface
{
	
	/**
	 * The subject of the triple object.
	 * 
	 * @return string
	 */
	public function getSubject() : string;
	
	/**
	 * The predicate of the triple object.
	 * 
	 * @return string
	 */
	public function getPredicate() : string;
	
	/**
	 * The object of the triple object.
	 * 
	 * @return ?string
	 */
	public function getObject() : ?string;
	
}
