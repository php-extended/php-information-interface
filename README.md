# php-extended/php-information-interface

A library to specify transport objects for individual informations that transit  between two systems.

![coverage](https://gitlab.com/php-extended/php-information-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-information-interface ^18`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-information`](https://gitlab.com/php-extended/php-information).

### InformationDataInterface

The `InformationDataInterface` is the most basic data that can transit.

It represents a simple information that an object can own.

### InformationRelationInterface

The `InformationRelationInterface` is the most basic relation that can
transit.

It represents a simple link between two objects.

### InformationTripleInterface

The `InformationTripleInterface` is used for semantic-web informations.
It is composed of only three fields, the subject, the predicate, and the object
of the triple (or tuple).

It can represent information for a specific object or a relation between two 
objects or a reference list.

### InformationMultiInterface

The `InformationMultiInterface` is a complex information that carries
in one go multiple data and relation informations.

It represents a complete object full of its own informations.

### InformationVisitorInterface

The `InformationVisitorInterface` is the only object that can interact
with all the others information interfaces.

It represents a dispatcher that will process with specific paths each of the
available information interface.


## License

MIT (See [license file](LICENSE)).
